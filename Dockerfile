FROM ubuntu:14.04

MAINTAINER "Toni Perez" "apbautista@gmv.com"

# Dependencies
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y python3 python3-pip nano
RUN apt-get install -y git build-essential libffi-dev
RUN apt-get install -y python3-dev
RUN apt-get install -y redis-server

# Install IntelMQ
RUN pip3 install typing
RUN git clone https://github.com/certtools/intelmq.git /tmp/intelmq
WORKDIR /tmp/intelmq
RUN pip3 install .

# IntelMQ Config files
RUN mv /opt/intelmq/etc/examples/*.conf /opt/intelmq/etc/
COPY conf/* /opt/intelmq/etc/

# Custom bots
COPY bots/outputs/warden_file /tmp/intelmq/intelmq/bots/outputs/warden_file
COPY bots/BOTS /tmp/intelmq/intelmq/bots

# Apply bots changes
RUN pip3 install --upgrade -e .

# Start Script
COPY startMQ /opt/intelmq/bin/
WORKDIR /opt/intelmq
CMD ["sh", "/opt/intelmq/bin/startMQ"]


