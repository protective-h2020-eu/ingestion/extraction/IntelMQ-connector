# IntelMQ dockerized connector

This project contains a docker with an intelMQ installation and a custom bot for intelMQ. By the date, it is reading data automatically from 3 sources:

- Malc0de
- Spamhaus
- Malware Domain List

With the custom bot implemented, it tries to parse the events and convert it to IDEA format in order to get IDEA formatted events that warden-filer could pick and understand without problems.

First, clone this repository, and run:

```
docker-compose up --build
```

- In **conf** directory you will find all the config related to intelMQ (pipeline, runtime,...)

- **logs** directory is the folder where the logs of all running bots are stored.

- In **warden-file** you can find the parsed events in IDEA format.

- In **file-output** the events in raw format are stored in a .txt

In order to enter in the running container you can run:

```
docker exec -it intelmq_parser bash
```

If you want more info about IntelMQ you can check it at: https://github.com/certtools/intelmq#table-of-contents
