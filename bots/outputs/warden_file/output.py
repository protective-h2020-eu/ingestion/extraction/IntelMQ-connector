# -*- coding: utf-8 -*-
import io

from intelmq.lib.bot import Bot
from uuid import uuid4
import json
import datetime


class WardenOutputBot(Bot):
    counter = 0
    def ideaFromEvent(self,event):
        idea  = {
           "Format": "IDEA0",
           "ID": str(uuid4()),
           "CreateTime": event.get('time.source',None),
           "DetectTime": event.get('time.observation',None),
           #"WinStartTime": format_timestamp(),
           #"WinEndTime": format_timestamp(),
           #"EventTime": format_timestamp(),
           #"CeaseTime": format_timestamp(),
           "Category": [event.get("classification.type","")],
           # "Category": ["Abusive.Spam","Fraud.Copyright"],
           #"Category": [choice(["Abusive.Spam","Abusive.Harassment","Malware","Fraud.Copyright","Test","Fraud.Phishing","Fraud.Scam"]) for dummy in range(randint(1, 3))],
           #"Ref": ["cve:CVE-%s-%s" % (randstr(string.digits, 4), randstr()), "http://www.example.com/%s" % randstr()],
           "Confidence": event.get("feed.accuracy",0),
           "Note": event.get("feed.name",""),
           #"ConnCount": randint(0, 65535),
            #"ConnCount": choice([randint(0, 65535), "asdf"]),    # Send wrong event sometimes
           "Source": [
              {
                 "Type": [event.get("classification.type","")],
                 "IP4": [(event.get("source.ip",event.get("source.network","0.0.0.0"))).split("/")[0]],#if null it reports a problem
                 "Hostname": [event.get("source.local_hostname","")],
                 "Port": [event.get("source.port",0)],
                  "URL": event.get("source.fqdn","")
              }
           ],
           "Node": [
              {
                 "Name": "intelmq",
                 "Type": ["Data"],
                 "SW": [event.get("feed.provider")],
                  "Note": 'intelmq source the provider is %s and the url readed is %s' % (event.get("feed.provider",""),event.get("feed.url",""))
              }
           ]
        }

        return idea

    def generateFile(self):

        WardenOutputBot.counter = WardenOutputBot.counter +1
        filename = self.parameters.file + datetime.datetime.now().strftime("%Y%m%d%H%M%S") + str(WardenOutputBot.counter)+".idea"
        self.logger.debug("Opening %r file.", filename)
        file = io.open(filename, mode='at', encoding="utf-8")
        self.logger.debug("File %r is open.", self.parameters.file)
        return file

    def init(self):
        self.logger.info('Warden file init with %s logger level.' % self.parameters.logging_level)

    def process(self):
        event = self.receive_message()
        idea = self.ideaFromEvent(event)
        event_data = json.dumps(idea, ensure_ascii=False)
        try:
            file = self.generateFile()
            file.write(event_data)
            file.write("\n")
            file.flush()
        except FileNotFoundError:
            self.init()
        else:
            self.acknowledge_message()


BOT = WardenOutputBot
